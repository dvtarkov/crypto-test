from collections import OrderedDict
import numpy as np
import asyncio
import datetime


# Время пересчёта коэффициентов линейного уравнения в секундах
FINE_TUNING_UPDATE_TIME = 60 * 60

# Время чистки логов в секундах
LOG_CLEAR_TIME = 60 * 90

# Промежуток времени в котором сохраняются логи
DELETE_TIME = datetime.timedelta(minutes=90)


class PriceLog:
    _log = OrderedDict()

    async def write_log(self, independent_price: float, dependent_price: float, abs_price: float) -> None:
        """
        Функция обновляет лог цен, добавляя в него текущие параметры.

        :param independent_price: Цена независимой криптовалюты (BTC)
        :param dependent_price: Цена зависимой криптовалюты (ETH)
        :param abs_price: Абсолютная цена зависимой криптовалюты без учёта независимой.
        :return: None
        """

        # Расчёт изменения цены
        if self._log:
            last_child = next(reversed(self._log.values()))
            abs_price_change = abs_price - float(last_child['abs_price'])
            price_change = dependent_price - float(last_child['dep_price'])

        else:
            abs_price_change = 0
            price_change = 0

        # Запись лога
        self._log[datetime.datetime.now()] = {
            'ind_price': independent_price,
            'dep_price': dependent_price,
            'abs_price': abs_price,
            'abs_price_change': abs_price_change,
            'price_change': price_change,
        }

    async def clear_log(self) -> None:
        """
        Очистка лога от старых данных.
        Раз в LOG_CLEAR_TIME удаляет все логи старше чем WARNING_TIME.
        :return: None
        """
        while True:
            delete_time = datetime.datetime.now() - DELETE_TIME
            log_keys = [i_key for i_key in self._log.keys()]
            for i_log in log_keys:
                if delete_time > i_log:
                    self._log.pop(i_log)
            await asyncio.sleep(LOG_CLEAR_TIME)

    async def linear_fine_tuning(self) -> None:
        """
        Поправка модели и коэффициентов линейной зависимости.
        Если количество записей более LOGS_TO_TUNE, программа производит перерасчёт показателей.
        И выводит их пользователю для возможностей дальнейшей корректировки
        :return:
        """
        while True:
            if self._log.__len__() > 100:
                await self._linear_resolve()
            await asyncio.sleep(FINE_TUNING_UPDATE_TIME)

    async def _linear_resolve(self) -> None:
        """
        Метод наименьших квадратов для переопределения коэффициентов на заданном интервале.
        :return: None
        """
        x_list = np.array([float(i_log['ind_price']) for i_log in self._log.values()])
        y_list = np.array([float(i_log['dep_price']) for i_log in self._log.values()])

        coefficients = np.polyfit(x_list, y_list, 1)
        print(coefficients)

    async def up_to_now(self, from_datetime: datetime.datetime) -> list:
        """
        Выдаёт значения log начиная с from_datetime и заканчивая текущим datatime
        :param from_datetime: время начиная с которого выбираются объекты
        :return: list список словарей выбранных объектов [{key, value}, ]
        """

        items = [{'key': i_key, 'value': i_value} for i_key, i_value in self._log.items() if i_key > from_datetime]
        return items

    async def get_by_key(self, key) -> dict:
        """
        Геттер, выдаёт значения log по ключу
        :param key: Ключ по которому ведется поиск
        :return: dict значений объекта
        """
        return self._log[key]
