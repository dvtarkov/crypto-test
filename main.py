import asyncio
import datetime
from price_log import PriceLog
from aiohttp import ClientSession


# URL API
API_URL = "https://fapi.binance.com"

# Токен базовой(Независимой) криптовалюты
INDEPENDENT_TOKEN = "BTCUSDT"
# Токен искомой(Зависимой) криптовалюты
DEPENDENT_TOKEN = "ETHUSDT"

# Коэффициенты для линейного уравнения (Расчитаны по 20 значениям на протяжении последнего месяца)
K = 0.0790687
B = 184.189


# Промежуток времени за который сравнивается цена
WARNING_TIME = datetime.timedelta(minutes=60)

# Время обновления проверки изменения цен (секунды)
WARNING_UPDATE_TIME = 5 * 60

# При каком проценте выводить предупреждение (проценты)
WARNING_PERCENT = 1

# Сообщение предупреждения
WARNING_MESSAGE = '!!! Изменение собственной стоимости {token}: {a_per}% за время {time}\n' \
                  '!!! Изменение полной стоимости: {d_per}%\n'


async def y(x: float = 1., b: float = B, k: float = K) -> float:
    """Функция рассчета линейной функции по заданным коэффициентам"""
    return k * x - b


async def get_price(api_url: str, token: str) -> dict:
    """
    Ассинхронная функция для запроса к API по средствам ClientSession
    :param api_url: путь до api
    :param token: ключ криптовалюты
    :return: словарь десериализованного JSON response.
    """
    url = f"{api_url}/fapi/v1/ticker/price?symbol={token}"
    async with ClientSession() as session:
        async with session.get(url=url) as response:
            assert response.status == 200
            return await response.json()


async def absolute_price_loop(independent_token: str, dependent_token: str, log: PriceLog) -> None:
    """
    Цикл обновления текущих цен на фьючерсы
    :param independent_token: Токен независимой криптовалюты (BTC)
    :param dependent_token: Токен зависимой(искомой) криптовалюты (ETH)
    :param log: объект класса PriceLog для хранения информации.
    :return: None
    """
    while True:
        independent = await get_price(API_URL, independent_token)
        dependent = await get_price(API_URL, dependent_token)
        abs_price = float(dependent['price']) - await y(float(independent['price']))
        await log.write_log(float(independent['price']), float(dependent['price']), abs_price)


async def get_price_change(log: PriceLog) -> None:
    """
    Ассинхронная функция для проверки изменения цен за период WARNING_TIME.
    Достаёт из лога все последние цены за промежуток, берёт точку отсчёта (Самую старую цену) и проверяет на сколько
    процентов изменилась цена. Выводит абсолютное и относительное изменение цен, если абсолютное изменение было больше
    чем WARNING_PERCENT.
    :param log: объект класса PriceLog для получения информации о предыдущих ценах.
    :return: None
    """
    while True:
        log_items = await log.up_to_now(datetime.datetime.now() - WARNING_TIME)
        if log_items:
            start_datetime = log_items[0]['key']
            older_log = await log.get_by_key(start_datetime)

            # Время в сообщении будет WARNING_TIME если расчёт был произведён за полный необходимый цикл
            # или время которое прошло с момента самого старого сообщения до настоящего времени.
            if WARNING_TIME <= datetime.datetime.now() - start_datetime:
                message_time = WARNING_TIME
            else:
                message_time = datetime.datetime.now() - start_datetime

            # Получение цены отсчёта
            base_price_dependent = float(older_log['dep_price'])
            base_price_absolute = float(older_log['abs_price'])

            # Получение полного изменения цен
            abs_price_change = sum(i_log['value']['abs_price_change'] for i_log in log_items)
            price_change = sum(i_log['value']['price_change'] for i_log in log_items)

            # Расчёт процента изменения
            abs_percent = abs_price_change / base_price_absolute * 100
            percent = price_change / base_price_dependent * 100

            if abs(abs_percent) >= WARNING_PERCENT:
                print(WARNING_MESSAGE.format(
                        token=DEPENDENT_TOKEN,
                        a_per=round(abs_percent, 3),
                        time=message_time,
                        d_per=round(percent, 3),
                      ))

        await asyncio.sleep(WARNING_UPDATE_TIME)


async def main() -> None:
    """Собирает все ассинхронные задачи и запускает их"""
    log = PriceLog()
    tasks = [
        asyncio.create_task(absolute_price_loop(INDEPENDENT_TOKEN, DEPENDENT_TOKEN, log)),
        asyncio.create_task(log.clear_log()),
        asyncio.create_task(log.linear_fine_tuning()),
        asyncio.create_task(get_price_change(log)),
    ]

    await asyncio.gather(*tasks)

if __name__ == '__main__':
    asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(main())
